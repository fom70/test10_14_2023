from flask import Flask, render_template

app = Flask(__name__)


@app.route('/')
def print_hello():
    return "Hello!"


@app.route('/text/')
def print_text():
    return "<h1>Some text!</h1>"

